CREATE TABLE product
(
	id serial NOT NULL,
	amount float NOT NULL,
	name varchar(255) NOT NULL,
    	unit varchar (255),
    	UNIQUE(name),
	PRIMARY KEY(id)
);

CREATE TABLE client
(
    id serial NOT NULL,
    first_name varchar(255) NOT NULL,
    surname varchar(255) NOT NULL,
    discount float,
    PRIMARY KEY(id)
);

CREATE TABLE meal
(
    id serial NOT NULL,
    type varchar (50),
    name varchar(255) NOT NULL,
    description varchar(255),
    price float NOT NULL,
    amount float NOT NULL,
    unit varchar (255),
    availabillity boolean NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE employee
(
    id serial NOT NULL,
    manager_id int,
    position varchar(50) NOT NULL,
    first_name varchar(50) NOT NULL,
    last_name varchar(50) NOT NULL,
    pesel varchar CHECK (LENGTH(pesel)=11) NOT NULL,
    salary float NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(manager_id) REFERENCES employee(id)
);

CREATE TABLE bill
(
    id serial NOT NULL,
    client_id int,
    employee_id int NOT NULL,
    status varchar (50) NOT NULL,
    date timestamp NOT NULL,
    total_cost float NOT NULL,
    tip float,
	bill_discount float,
    PRIMARY KEY(id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(employee_id) REFERENCES employee(id)
);


