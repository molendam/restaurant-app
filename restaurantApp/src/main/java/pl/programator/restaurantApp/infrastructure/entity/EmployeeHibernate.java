package pl.programator.restaurantApp.infrastructure.entity;

import pl.programator.restaurantApp.domain.model.Position;

import javax.persistence.*;
import java.util.List;

//powiązania z employee dodać
@Entity
@Table (name = "employee")
public class EmployeeHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    // lista pracowników : jeden manager może mieć wielu pracowników pod sobą
    // proponowany fetchtype: lazy - nie zawsze chcemy przeglądać pracowników których ma manager
    // usunięcie pracownika oznacza
    // uaktualnienie usunięcie pracownika, widoczne również tutaj, cascade ALL
    @OneToMany (mappedBy = "managerId", fetch = FetchType.LAZY, cascade = CascadeType.ALL )
    private List<EmployeeHibernate> employees;

    @ManyToOne
    @JoinColumn (name = "manager_id")
    private EmployeeHibernate managerId;

    @OneToMany (mappedBy = "employee")
    private List<BillHibernate> bills;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Position position;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column( length = 11, nullable = false)
    private String pesel;

    @Column(nullable = false)
    private Float salary;

    public EmployeeHibernate() {
    }

    public EmployeeHibernate(
            Long id,
            Position position,
            String firstName,
            String lastName,
            String pesel,
            EmployeeHibernate managerId,
            Float salary) {
        //super(id);
        this.id = id;
        this.position = position;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.managerId = managerId;
        this.salary = salary;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<EmployeeHibernate> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeHibernate> employees) {
        this.employees = employees;
    }

    public List<BillHibernate> getBills() {
        return bills;
    }

    public void setBills(List<BillHibernate> bills) {
        this.bills = bills;
    }

    public EmployeeHibernate getManagerId() {
        return managerId;
    }

    public void setManagerId(EmployeeHibernate managerId) {
        this.managerId = managerId;
    }


    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }
}
