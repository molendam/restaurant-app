package pl.programator.restaurantApp.infrastructure.entity;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "product")
public class ProductHibernate {

@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private Float amount;

    @Column(unique = true, nullable = false)
    private String name;

    private String unit;

    public ProductHibernate() {
    }

    public ProductHibernate(Long id, Float amount, String name, String unit) {
        this.id=id;
        //super(id);
        this.amount = amount;
        this.name = name;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ProductHibernate.class.getSimpleName() + "[", "]")
                .add("amount=" + amount)
                .add("name='" + name + "'")
                .add("unit='" + unit + "'")
                .add("id=" + id)
                .toString();
    }
}
