package pl.programator.restaurantApp.infrastructure.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "client")
public class ClientHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;


    @Column(name = "first_name")
    @NotNull
    private String firstName;

    @NotNull
    private String surname;

    private Float discount;

    //usunięcie klienta usuwa jego rachunki?
    @OneToMany(mappedBy = "client", cascade = CascadeType.PERSIST)//cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BillHibernate> bills;

    public ClientHibernate() {
    }

    public ClientHibernate(
            Long id,
            String firstName,
            String surname,
            Float discount) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public List<BillHibernate> getBills() {
        if (bills == null) {
            bills = new ArrayList<>();
        }
        return bills;
    }

    public void setBills(List<BillHibernate> bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ClientHibernate.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("surname='" + surname + "'")
                .add("discount=" + discount)
                .add("id=" + id)
                .toString();
    }
}
