package pl.programator.restaurantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.programator.restaurantApp.domain.model.Employee;
import pl.programator.restaurantApp.domain.model.Position;
import pl.programator.restaurantApp.domain.service.repository.EmployeeRepository;
import pl.programator.restaurantApp.infrastructure.entity.BillHibernate;
import pl.programator.restaurantApp.infrastructure.entity.EmployeeHibernate;
import pl.programator.restaurantApp.infrastructure.repository.EmployeeHibernateRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final EmployeeHibernateRepository employeeHibernateRepository;

    @Autowired
    public EmployeeRepositoryImpl(EmployeeHibernateRepository employeeHibernateRepository) {
        this.employeeHibernateRepository = employeeHibernateRepository;
    }

    @Override
    public Employee createEmployee(
            Position position,
            String firstName,
            String lastName,
            String pesel,
            Long managerId,
            Float salary) {

        EmployeeHibernate employeeHibernate = new EmployeeHibernate(
                null,
                position,
                firstName,
                lastName,
                pesel,
                null,
                salary);
        if (managerId!=null){
            employeeHibernate.setManagerId(employeeHibernateRepository.findById(managerId).get());
        }

        employeeHibernateRepository.save(employeeHibernate);
        return toDomain(employeeHibernate);
    }

    public Employee toDomain(EmployeeHibernate employeeHibernate) {
        if (employeeHibernate == null) return null;
        Employee employee = new Employee(
                employeeHibernate.getId(),
                employeeHibernate.getFirstName(),
                employeeHibernate.getLastName(),
                employeeHibernate.getPesel(),
                null,
                null,
                employeeHibernate.getPosition(),
                employeeHibernate.getSalary());
        if (employeeHibernate.getManagerId() != null) {
            employee.setManagerId(employeeHibernate.getManagerId().getId());
        }
        if(employeeHibernate.getBills()!=null){
           employee.setBillIds(
                   employeeHibernate
                           .getBills()
                           .stream()
                           .map(BillHibernate::getId)
                           .collect(Collectors.toList())) ;
        }
        return employee;
    }

    @Override
    public Optional<Employee> getEmployee(Long id) {
        return employeeHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Employee> getAllEmployees(Pageable pageable) {
        return employeeHibernateRepository.
                findAll(pageable).
                stream().
                map(this::toDomain).
                collect(Collectors.toList());
    }

    @Override
    public void removeEmployee(Long id) {
        employeeHibernateRepository.deleteById(id);
    }

    public EmployeeHibernate toHibernate(Employee employee) {
        if (employee == null) return null;
        EmployeeHibernate employeeHibernate = new EmployeeHibernate(
                employee.getId(),
                employee.getPosition(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getPesel(),
                null
                ,
                employee.getSalary());
        if (employee.getManagerId() != null) {
            Optional<EmployeeHibernate> optionalEmployee = employeeHibernateRepository.
                    findById(employee.getManagerId());
            if (optionalEmployee.isPresent()) {
                employeeHibernate.setManagerId(optionalEmployee.get());
            }
        }
        return employeeHibernate;
    }
}
