package pl.programator.restaurantApp.infrastructure.adapter;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.programator.restaurantApp.domain.model.Product;
import pl.programator.restaurantApp.domain.service.repository.ProductRepository;
import pl.programator.restaurantApp.infrastructure.entity.ProductHibernate;
import pl.programator.restaurantApp.infrastructure.repository.ProductHibernateRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository

public class ProductRepositoryImpl implements ProductRepository {

    private ProductHibernateRepository productHibernateRepository;

    @Autowired
    public ProductRepositoryImpl(ProductHibernateRepository productHibernateRepository) {
        this.productHibernateRepository = productHibernateRepository;
    }

    @Override
    public Product createProduct(Float amount, String name, String unit) {
        ProductHibernate productHibernate = new ProductHibernate(null, amount, name, unit);
        productHibernateRepository.save(productHibernate);
        System.out.println(productHibernate.getId());
        Product p = toDomain(productHibernate);
        return toDomain(productHibernate);
    }

    @Override
    public Optional<Product> getProduct(Long id) {
        Optional<Product> product = productHibernateRepository.findById(id).map(this::toDomain);
        return product;
    }

    @Override
    public List<Product> getAllProducts(Pageable pageable) {
        Page<ProductHibernate> page = productHibernateRepository.findAll(pageable);
        List<ProductHibernate> hibernates = page.getContent();
        return hibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeProduct(Long id) {
    productHibernateRepository.deleteById(id);
    }

    public Product toDomain(ProductHibernate productHibernate) {
        return new Product(
                productHibernate.getId(),
                productHibernate.getAmount(),
                productHibernate.getName(),
                productHibernate.getUnit());
    }
}
