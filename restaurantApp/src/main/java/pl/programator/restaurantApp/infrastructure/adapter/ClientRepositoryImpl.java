package pl.programator.restaurantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.programator.restaurantApp.domain.model.Client;
import pl.programator.restaurantApp.domain.service.repository.ClientRepository;
import pl.programator.restaurantApp.infrastructure.entity.ClientHibernate;
import pl.programator.restaurantApp.infrastructure.repository.ClientHibernateRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    private ClientHibernateRepository clientHibernateRepository;

    @Autowired
    public ClientRepositoryImpl(ClientHibernateRepository clientHibernateRepository) {
        this.clientHibernateRepository = clientHibernateRepository;
    }

    @Override
    public Client createClient(String firstName, String surname, Float discount) {
        ClientHibernate clientHibernate = new ClientHibernate(null, firstName, surname, discount);
        clientHibernateRepository.save(clientHibernate);
        return toDomain(clientHibernate);
    }

    @Override
    public Optional<Client> getClient(Long id) {
        return clientHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Client> getAllClients(Pageable pageable) {
        Page<ClientHibernate> page = clientHibernateRepository.findAll(pageable);
        List<ClientHibernate> hibernates = page.getContent();
        return hibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeClient(Long id) {
        clientHibernateRepository.deleteById(id);

    }

    public Client toDomain(ClientHibernate clientHibernate) {
        if(clientHibernate == null) return null;
        return new Client(
                clientHibernate.getId(),
                clientHibernate.getFirstName(),
                clientHibernate.getSurname(),
                clientHibernate.getDiscount());
    }

    public ClientHibernate toHibernate(Client client) {
        if(client == null) return null;
        return new ClientHibernate(
                client.getId(),
                client.getFirstName(),
                client.getSurname(),
                client.getDiscount());
    }
}
