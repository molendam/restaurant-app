package pl.programator.restaurantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.programator.restaurantApp.domain.model.*;
import pl.programator.restaurantApp.domain.service.repository.BillRepository;
import pl.programator.restaurantApp.infrastructure.entity.BillHibernate;
import pl.programator.restaurantApp.infrastructure.entity.ClientHibernate;
import pl.programator.restaurantApp.infrastructure.entity.EmployeeHibernate;
import pl.programator.restaurantApp.infrastructure.entity.MealHibernate;
import pl.programator.restaurantApp.infrastructure.repository.BillHibernateRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class BillRepositoryImpl implements BillRepository {

    private final BillHibernateRepository billHibernateRepository;
    private final MealRepositoryImpl mealRepository;
    private final ClientRepositoryImpl clientRepository;
    private final EmployeeRepositoryImpl employeeRepository;

    @Autowired
    public BillRepositoryImpl(BillHibernateRepository billHibernateRepository, MealRepositoryImpl mealRepository, ClientRepositoryImpl clientRepository, EmployeeRepositoryImpl employeeRepository) {
        this.billHibernateRepository = billHibernateRepository;
        this.mealRepository = mealRepository;
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Bill createBill(
            Long employeeId,
            List<Long> mealIdsList,
            Client client,
            LocalDateTime date,
            Float totalCost,
            Float billDiscount,
            Float tip,
            Status status) {


        Bill bill = new Bill(
                client,
                employeeRepository.getEmployee(employeeId).get(),
                date,
                totalCost,
                billDiscount,
                tip,
                status,
                mealIdsList
        );


        BillHibernate billHibernate = billHibernateRepository.save(toHibernate(bill));
        return toDomain(billHibernate);
    }


    @Override
    public Optional<Bill> getBill(Long id) {
        return billHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Bill> getAllBills(Pageable pageable) {
        return billHibernateRepository.
                findAll(pageable).
                stream().
                map(this::toDomain).
                collect(Collectors.toList());
    }

    @Override
    public void removeBill(Long id) {
        billHibernateRepository.deleteById(id);
    }

    @Override
    public Bill save(Bill bill) {
        BillHibernate billHibernate = billHibernateRepository.save(toHibernate(bill));
        return this.toDomain(billHibernate);
    }

    public Bill toDomain(BillHibernate billHibernate) {
        return new Bill(
                billHibernate.getId(),
                clientRepository.toDomain(billHibernate.getClient()) ,
                employeeRepository.toDomain(billHibernate.getEmployeeHibernate()),
                billHibernate.getDate(),
                billHibernate.getTotalCost(),
                billHibernate.getBillDiscount(),
                billHibernate.getTip(),
                billHibernate.getStatus(),
                billHibernate.getMealHibernateList().stream().map(MealHibernate::getId).collect(Collectors.toList())
        );
    }

    private BillHibernate toHibernate(Bill bill) {
        return new BillHibernate(
                bill.getId(),
                employeeRepository.toHibernate(bill.getEmployee()),
                mealRepository.toMealHibernateList(bill.getMealIds()),
                clientRepository.toHibernate(bill.getClient()),
                bill.getDate(),
                bill.getTotalCost(),
                bill.getBillDiscount(),
                bill.getTip(),
                bill.getStatus());
    }

}
