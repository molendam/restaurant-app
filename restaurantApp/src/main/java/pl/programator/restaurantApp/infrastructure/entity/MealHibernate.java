package pl.programator.restaurantApp.infrastructure.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "meal")
public class MealHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;


    @Column(length = 50)
    private String type;

    @Column(nullable = false)
    private String name;

    private String description;

    @Column(nullable = false)
    private Float price;

    @Column(nullable = false)
    private Float amount;

    private String unit;

    @Column(nullable = false)
    private Boolean availabillity;

    //cascade type ustalić
    @ManyToMany(mappedBy = "mealHibernateList")
    private List<BillHibernate> bills;

    public MealHibernate() {
    }

    public MealHibernate(Long id,
                         String type,
                         String name,
                         String description,
                         Float price,
                         Float amount,
                         String unit,
                         Boolean availabillity) {

        //super(id);
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.price = price;
        this.amount = amount;
        this.unit = unit;
        this.availabillity = availabillity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getAvailabillity() {
        return availabillity;
    }

    public void setAvailabillity(Boolean availabillity) {
        this.availabillity = availabillity;
    }

    public List<BillHibernate> getBills() {
        return bills;
    }

    public void setBills(List<BillHibernate> bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MealHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("type='" + type + "'")
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("amount=" + amount)
                .add("unit='" + unit + "'")
                .add("availabillity=" + availabillity)
                .toString();
    }
}
