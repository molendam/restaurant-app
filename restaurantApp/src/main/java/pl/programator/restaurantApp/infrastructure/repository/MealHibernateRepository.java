package pl.programator.restaurantApp.infrastructure.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.programator.restaurantApp.infrastructure.entity.MealHibernate;

public interface MealHibernateRepository extends PagingAndSortingRepository<MealHibernate,Long> {
}
