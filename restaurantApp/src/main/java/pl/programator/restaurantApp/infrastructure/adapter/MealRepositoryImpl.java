package pl.programator.restaurantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.programator.restaurantApp.domain.model.Meal;
import pl.programator.restaurantApp.domain.service.repository.MealRepository;
import pl.programator.restaurantApp.infrastructure.entity.MealHibernate;
import pl.programator.restaurantApp.infrastructure.repository.MealHibernateRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class MealRepositoryImpl implements MealRepository {

    private MealHibernateRepository mealHibernateRepository;

    @Autowired
    public MealRepositoryImpl(MealHibernateRepository mealHibernateRepository) {
        this.mealHibernateRepository = mealHibernateRepository;
    }

    @Override
    public Meal createMeal(
            String type,
            String name,
            String description,
            Float price,
            Float amount,
            String unit,
            Boolean availabillity) {
        MealHibernate mealHibernate = new MealHibernate(null, type, name, description, price, amount, unit, availabillity);
        mealHibernateRepository.save(mealHibernate);
        return toDomain(mealHibernate);
    }

    @Override
    public Optional<Meal> getMeal(Long id) {
        return mealHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Meal> getAllMeals(Pageable pageable) {
        Page<MealHibernate> page = mealHibernateRepository.findAll(pageable);
        List<MealHibernate> hibernates = page.getContent();
        return hibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeMeal(Long id) {
        mealHibernateRepository.deleteById(id);
    }

    @Override
    public Long createMeal(Meal meal) {
        return mealHibernateRepository.save(toHibernate(meal)).getId();
    }

    public Meal toDomain(MealHibernate mealHibernate) {
        return new Meal(
                mealHibernate.getId(),
                mealHibernate.getType(),
                mealHibernate.getName(),
                mealHibernate.getDescription(),
                mealHibernate.getPrice(),
                mealHibernate.getAmount(),
                mealHibernate.getUnit(),
                mealHibernate.getAvailabillity());
    }

    public MealHibernate toHibernate(Meal meal) {
        return new MealHibernate(
                meal.getId(),
                meal.getType(),
                meal.getName(),
                meal.getDescription(),
                meal.getPrice(),
                meal.getAmount(),
                meal.getUnit(),
                meal.getAvailabillity()
        );
    }
    //sprawdzac optionala jak wiadomo, że będą?
    public List<MealHibernate> toMealHibernateList(List<Long> mealHibernateList) {

        List <Optional<MealHibernate>> optionalMealsHibernate = mealHibernateList.stream().
                map(x ->mealHibernateRepository.findById(x)).
                collect(Collectors.toList());

        return optionalMealsHibernate.stream().filter(Optional::isPresent).
                map(Optional::get).collect(Collectors.toList());


        /*List<MealHibernate> mealHibernates = new ArrayList<>();
        for (Long mealId : mealHibernateList
        ) {
            mealHibernates.add(toHibernate(getMeal(mealId).get()));
        }*/
        //return mealHibernates;
    }
}
