package pl.programator.restaurantApp.infrastructure.entity;

import org.hibernate.annotations.CreationTimestamp;
import pl.programator.restaurantApp.domain.model.Client;
import pl.programator.restaurantApp.domain.model.Status;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "bill")
public class BillHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientHibernate client;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private EmployeeHibernate employee;

    @CreationTimestamp
    @Column(nullable = false)
    private LocalDateTime date;

    @Column(name = "total_cost", nullable = false)
    private Float totalCost;

    @Column(name = "bill_discount")
    private Float billDiscount;

    private Float tip;

    @Column(nullable = false)
    private Status status;


    @ManyToMany
    @JoinTable(
            name = "meal_bill",
            joinColumns = @JoinColumn(name = "bill_id"),
            inverseJoinColumns =@JoinColumn(name = "meal_id"))
    private List<MealHibernate> mealHibernateList;

    public BillHibernate() {
    }

    public BillHibernate(Long id,
                         EmployeeHibernate employeeHibernate,
                         List<MealHibernate> mealHibernateList,
                         ClientHibernate client,
                         LocalDateTime date,
                         Float totalCost,
                         Float billDiscount,
                         Float tip,
                         Status status) {
        this.id = id;
        this.employee = employeeHibernate;
        this.mealHibernateList = mealHibernateList;
        this.client = client;
        this.date = date;
        this.totalCost = totalCost;
        this.billDiscount = billDiscount;
        this.tip = tip;
        this.status = status;
//        for(MealBillHibernate mealBillHibernate : mealBill) mealBillHibernate.setBill(this);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmployeeHibernate getEmployeeHibernate() {
        return employee;
    }

    public void setEmployeeHibernate(EmployeeHibernate employeeHibernate) {
        this.employee = employeeHibernate;
    }

    public ClientHibernate getClient() {
        return client;
    }

    public void setClient(ClientHibernate client) {
        this.client = client;
    }

    public void setEmployeeId(EmployeeHibernate employeeHibernate) {
        this.employee = employeeHibernate;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Float totalCost) {
        this.totalCost = totalCost;
    }

    public Float getBillDiscount() {
        return billDiscount;
    }

    public void setBillDiscount(Float billDiscount) {
        this.billDiscount = billDiscount;
    }

    public Float getTip() {
        return tip;
    }

    public void setTip(Float tip) {
        this.tip = tip;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<MealHibernate> getMealHibernateList() {
        return mealHibernateList;
    }

    public void setMealHibernateList(List<MealHibernate> mealHibernateList) {
        this.mealHibernateList = mealHibernateList;
    }

}
