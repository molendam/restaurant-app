package pl.programator.restaurantApp.controller.dto;

import pl.programator.restaurantApp.domain.model.Client;

import java.io.Serializable;

public class ClientDTO implements Serializable {

    private String firstName;
    private String surname;
    private Float discount;
    private Long id;

    public ClientDTO(
            Long id,
            String firstName,
            String surname,
            Float discount) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.discount = discount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public static ClientDTO toDTO(Client client){
        return new ClientDTO(
                client.getId(),
                client.getFirstName(),
                client.getSurname(),
                client.getDiscount());
    }

    public Client toDomain() {
        return new Client(
                this.getId(),
                this.getFirstName(),
                this.getSurname(),
                this.getDiscount());
    }
}
