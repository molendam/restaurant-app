package pl.programator.restaurantApp.controller.dto;

import pl.programator.restaurantApp.domain.model.Meal;

import java.io.Serializable;

public class MealDTO implements Serializable {

    private Long id;
    private String type;
    private String name;
    private String description;
    private Float price;
    private Float amount;
    private String unit;
    private Boolean availabillity;

    public MealDTO(
            Long id,
            String type,
            String name,
            String description,
            Float price,
            Float amount,
            String unit,
            Boolean availabillity) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.price = price;
        this.amount = amount;
        this.unit = unit;
        this.availabillity = availabillity;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getAvailabillity() {
        return availabillity;
    }

    public void setAvailabillity(Boolean availabillity) {
        this.availabillity = availabillity;
    }

    public static MealDTO toDTO(Meal meal) {
        return new MealDTO(
                meal.getId(),
                meal.getType(),
                meal.getName(),
                meal.getDescription(),
                meal.getPrice(),
                meal.getAmount(),
                meal.getUnit(),
                meal.getAvailabillity());
    }

    public Meal toDomain() {
        return new Meal(
                this.id,
                this.type,
                this.name,
                this.description,
                this.price,
                this.amount,
                this.unit,
                this.availabillity
        );
    }
}
