package pl.programator.restaurantApp.controller.dto;

import pl.programator.restaurantApp.domain.model.Employee;
import pl.programator.restaurantApp.domain.model.Position;

import java.io.Serializable;
import java.util.List;

public class EmployeeDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String pesel;
    private List<Long> employees;
    private Long managerId;
    private List<Long> billsIds;
    private Position position;
    private Float salary;

    public EmployeeDTO(
            Long id,
            String firstName,
            String lastName,
            String pesel,
            Long managerId,
            Position position,
            Float salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.managerId=managerId;
        /*this.billsIds = billsIds;*/
        this.position = position;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public List<Long> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Long> employees) {
        this.employees = employees;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getBillsIds() {
        return billsIds;
    }

    public void setBillsIds(List<Long> billsIds) {
        this.billsIds = billsIds;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public static EmployeeDTO toDTO (Employee employee)
    {
        return new EmployeeDTO(
                employee.getId(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getPesel(),
                employee.getManagerId(),
                employee.getPosition(),
                employee.getSalary());
    }

    public Employee toDomain(){
        return new Employee(
                this.firstName,
                this.lastName,
                this.pesel,
                this.managerId,
                this.billsIds,
                this.position,
                this.salary
        );
    }

}
