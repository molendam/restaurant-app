package pl.programator.restaurantApp.controller.dto;

import pl.programator.restaurantApp.domain.model.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BillDTO implements Serializable {

    private Long id;
    private Long clientId;
    private Long employeeId;
    private LocalDateTime date;
    private Float totalCost;
    private Float billDiscount;
    private Float tip;
    private Status status;
    private List<Long> mealsIds;

    public BillDTO(
            Long id,
            Long clientId,
            Long employeeId,
            LocalDateTime date,
            Float totalCost,
            Float billDiscount,
            Float tip,
            Status status,
            List<Long> mealsIds) {
        this.id = id;
        this.clientId = clientId;
        this.employeeId = employeeId;
        this.date = date;
        this.totalCost = totalCost;
        this.billDiscount = billDiscount;
        this.tip = tip;
        this.status = status;
        this.mealsIds = mealsIds;
    }


    public Long getClientId() {
        return clientId;
    }

    public void setClient(Long clientId) {
        this.clientId = clientId;
    }

    public Long getEmployee() {
        return employeeId;
    }

    public void setEmployee(Long employeeId) {
        this.employeeId = employeeId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Float totalCost) {
        this.totalCost = totalCost;
    }

    public Float getBillDiscount() {
        return billDiscount;
    }

    public void setBillDiscount(Float billDiscount) {
        this.billDiscount = billDiscount;
    }

    public Float getTip() {
        return tip;
    }

    public void setTip(Float tip) {
        this.tip = tip;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public List<Long> getMealsIds() {
        if(mealsIds == null){
            mealsIds = new ArrayList<>();
        }
        return mealsIds;
    }

    public void setMealsIds(List<Long> mealsIds) {
        this.mealsIds = mealsIds;
    }

    public static BillDTO toDTO(Bill bill) {
            BillDTO billDTO = new BillDTO(
                    bill.getId(),
                    null,
                    bill.getEmployee().getId(),
                    bill.getDate(),
                    bill.getTotalCost(),
                    bill.getBillDiscount(),
                    bill.getTip(),
                    bill.getStatus(),
                    bill.getMealIds()
            );
            if (bill.getClient()!=null)
                billDTO.setClient(bill.getClient().getId());
            return billDTO;

    }
    public Bill toDomain(){
        return new Bill(
                this.id,
                null,
                null,
                this.date,
                this.totalCost,
                this.billDiscount,
                this.tip,
                this.status,
                this.mealsIds
        );
    }
}
