package pl.programator.restaurantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.programator.restaurantApp.controller.dto.EmployeeDTO;
import pl.programator.restaurantApp.domain.model.Employee;
import pl.programator.restaurantApp.domain.service.EmployeeService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(value = "/get/{id}")
    public EmployeeDTO getEmployee(@PathVariable("id") Long id) {
        Optional<Employee> EmployeeOptional = employeeService.getEmployee(id);
        return EmployeeDTO.toDTO(EmployeeOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<EmployeeDTO> getAllEmployee() {
        List<Employee> Employees = employeeService.getAllEmployees(Pageable.unpaged());
        return Employees.stream().map(EmployeeDTO::toDTO).collect(Collectors.toList());
    }

    @PostMapping(value = "/add")
    public Long createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.createEmployee(employeeDTO.toDomain());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.removeEmployee(id);
    }
}
