package pl.programator.restaurantApp.controller;

import org.hibernate.annotations.SelectBeforeUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.programator.restaurantApp.controller.dto.ProductDTO;
import pl.programator.restaurantApp.domain.model.Product;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.ProductService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/get/{id}")
    public ProductDTO getProduct(@PathVariable("id") Long id) {
        Optional<Product> productOptional = productService.getProduct(id);
        return ProductDTO.toDTO(productOptional.get());
    }

    @PostMapping(value = "/add")
    public Long createProduct(@RequestBody ProductDTO productDTO) {
        return productService.createProduct(productDTO.toDomain());
    }

    @GetMapping(value = "/getAll")
    public List<ProductDTO> getAllProduct() {
        List<Product> products = productService.getAllProducts(Pageable.unpaged());
        return products.stream().map(ProductDTO::toDTO).collect(Collectors.toList());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.removeProduct(id);
    }

    @PutMapping(value = "/update/{id}")
    public void updateProduct(@PathVariable Long id, @RequestBody ProductDTO productDTO){
        productService.updateProduct(id, productDTO.toDomain());

    }
}
