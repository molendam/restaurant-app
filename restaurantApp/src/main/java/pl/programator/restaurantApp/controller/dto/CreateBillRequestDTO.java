package pl.programator.restaurantApp.controller.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CreateBillRequestDTO implements Serializable {
     private List<Long> mealIdsList;
     private Long employeeId;


    public CreateBillRequestDTO(List<Long> mealIdsList, Long employeeId) {
        this.mealIdsList = mealIdsList;
        this.employeeId = employeeId;
    }

    public List<Long> getMealIdsList() {
        if (mealIdsList==null){
            mealIdsList = new ArrayList<>();
        }
        return mealIdsList;
    }

    public void setMealIdsList(List<Long> mealIdsList) {
        this.mealIdsList = mealIdsList;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }


}
