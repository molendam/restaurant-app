package pl.programator.restaurantApp.controller.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddMealsRequestDTO implements Serializable {
    private List<Long> mealIdsList;
    private Long billId;

    public AddMealsRequestDTO(List<Long> mealIdsList, Long billId) {
        this.mealIdsList = mealIdsList;
        this.billId = billId;
    }

    public AddMealsRequestDTO() {
    }

    public List<Long> getMealIdsList() {
        return mealIdsList;
    }

    public void setMealIdsList(List<Long> mealIdsList) {
        this.mealIdsList = mealIdsList;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }
}
