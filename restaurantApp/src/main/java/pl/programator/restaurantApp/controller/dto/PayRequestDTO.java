package pl.programator.restaurantApp.controller.dto;

import java.io.Serializable;

public class PayRequestDTO implements Serializable {
    private Long billId;
    private Float tip;
    private Long clientId;

    public PayRequestDTO() {
    }

    public PayRequestDTO(Long billId, Float tip, Long clientId) {
        this.billId = billId;
        this.tip = tip;
        this.clientId = clientId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public Float getTip() {
        return tip;
    }

    public void setTip(Float tip) {
        this.tip = tip;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
}
