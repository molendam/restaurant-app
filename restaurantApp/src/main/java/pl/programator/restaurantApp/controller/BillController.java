package pl.programator.restaurantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.programator.restaurantApp.controller.dto.*;
import pl.programator.restaurantApp.domain.model.Bill;
import pl.programator.restaurantApp.domain.service.BillService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("bill")
public class BillController {
    private final BillService billService;

    @Autowired
    public BillController(BillService billService) {
        this.billService = billService;
    }

    @GetMapping(value = "/get/{id}")
    public BillDTO getBill(@PathVariable("id") Long id) {
        Optional<Bill> billOptional = billService.getBill(id);
        return BillDTO.toDTO(billOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<BillDTO> getAllBills() {
        return billService.getAllBills(Pageable
                .unpaged())
                .stream()
                .map(BillDTO::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/create")//produces application.json
    public Long createBill(@RequestBody CreateBillRequestDTO cbrDTO) {
        return billService.createBill(
                cbrDTO.getMealIdsList(),
                cbrDTO.getEmployeeId());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void removeBill(@PathVariable Long id) {
        billService.removeBill(id);
    }


    @PutMapping(value = "/addMeals")
    public void addMeals(@RequestBody AddMealsRequestDTO amrDTO) {
        billService.addMeals(
                amrDTO.getMealIdsList(),
                amrDTO.getBillId());
    }

    @PostMapping(value = "/pay")
    public BillDTO pay(@RequestBody PayRequestDTO prDTO) {
        return BillDTO.toDTO(billService.pay(
                prDTO.getBillId(),
                prDTO.getClientId(),
                prDTO.getTip()));
    }
}
