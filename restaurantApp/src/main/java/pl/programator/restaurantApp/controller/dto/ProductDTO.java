package pl.programator.restaurantApp.controller.dto;

import pl.programator.restaurantApp.domain.model.Product;

import java.io.Serializable;

public class ProductDTO implements Serializable {

    private Long id;

    private Float amount;

    private String name;

    private String unit;

    public ProductDTO() {
    }

    public ProductDTO(Long id, Float amount, String name, String unit) {
        this.id = id;
        this.amount = amount;
        this.name = name;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Product toDomain (){
        return new Product(id,amount,name,unit);
    }


    public static ProductDTO toDTO(Product product){
        return new ProductDTO(
                product.getId(),
                product.getAmount(),
                product.getName(),
                product.getUnit()
        );
    }
}
