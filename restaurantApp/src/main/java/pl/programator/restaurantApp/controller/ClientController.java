package pl.programator.restaurantApp.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.programator.restaurantApp.controller.dto.ClientDTO;
import pl.programator.restaurantApp.domain.model.Client;
import pl.programator.restaurantApp.domain.service.ClientService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(value = "/get/{id}")
    public ClientDTO getClient(@PathVariable("id") Long id) {
        Optional<Client> clientOptional = clientService.getClient(id);
        return ClientDTO.toDTO(clientOptional.get());
    }

    @GetMapping(value = "/getAll")
    public List<ClientDTO> getAllClient() {
        List<Client> clients = clientService.getAllClients(Pageable.unpaged());
        return clients.stream().map(ClientDTO::toDTO).collect(Collectors.toList());
    }

    @PostMapping(value = "/add")//produces application.json
    public Long createClient(@RequestBody ClientDTO clientDTO) {
        return clientService.createClient(clientDTO.toDomain());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteClient(@PathVariable Long id) {
        clientService.removeClient(id);
    }
}
