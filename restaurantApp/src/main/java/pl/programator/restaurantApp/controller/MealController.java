package pl.programator.restaurantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.programator.restaurantApp.controller.dto.MealDTO;
import pl.programator.restaurantApp.domain.model.Meal;
import pl.programator.restaurantApp.domain.service.MealService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("meal")
public class MealController {
    private final MealService mealService;

    @Autowired
    public MealController(MealService mealService) {
        this.mealService = mealService;
    }

    @GetMapping(value = "/get/{id}")
    public MealDTO getMeal(@PathVariable("id") Long id) { ;
        return MealDTO.toDTO(mealService.getMeal(id));
    }

    @GetMapping(value = "/getAll")
    public List<MealDTO> getAllMeal() {
        List<Meal> Meals = mealService.getAllMeals(Pageable.unpaged());
        return Meals.stream().map(MealDTO::toDTO).collect(Collectors.toList());
    }

    @PostMapping(value = "/add")//produces application.json
    public Long createMeal(@RequestBody MealDTO mealDTO) {
        return mealService.createMeal(mealDTO.toDomain());
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteMeal(@PathVariable Long id) {
        mealService.removeMeal(id);
    }
}
