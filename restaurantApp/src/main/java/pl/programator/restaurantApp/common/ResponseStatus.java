package pl.programator.restaurantApp.common;

public enum ResponseStatus {
    SUCCESS, ERROR
}
