package pl.programator.restaurantApp.common;

public class ConstErrorMsg {

    private String errorCode;
    private String errorMsg;

    public ConstErrorMsg(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
