package pl.programator.restaurantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Employee;
import pl.programator.restaurantApp.domain.model.Position;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Optional<Employee> getEmployee(Long id);

    List<Employee> getAllEmployees(Pageable pageable);

    void removeEmployee(Long id);

    Long createEmployee(Employee employee);
}


   /*Long createEmployee (Long managerId,
                             Position position,
                             String firstName,
                             String lastName,
                             String pesel,
                             Float salary);*/