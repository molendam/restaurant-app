package pl.programator.restaurantApp.domain.service.repository;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Client;

import java.util.List;
import java.util.Optional;

public interface ClientRepository {

    Client createClient (String firstName,
                         String surname,
                         Float discount);

    Optional<Client> getClient(Long id);

    List<Client> getAllClients(Pageable pageable);

    void removeClient(Long id);
}
