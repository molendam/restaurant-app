package pl.programator.restaurantApp.domain.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Bill {

    private Long id;
    private Client client;
    private Employee employee;
    private LocalDateTime date;
    private Float totalCost;
    private Float billDiscount;
    private Float tip;
    private Status status;
    private List<Long> mealIds;

    public Bill(
            Client client,
            Employee employee,
            LocalDateTime date,
            Float totalCost,
            Float billDiscount,
            Float tip,
            Status status,
            List<Long> mealIds) {
        this.client = client;
        this.employee = employee;
        this.date = date;
        this.totalCost = totalCost;
        this.billDiscount = billDiscount;
        this.tip = tip;
        this.status = status;
        this.mealIds = mealIds;
    }

    public Bill(
            Long id,
            Client client,
            Employee employee,
            LocalDateTime date,
            Float totalCost,
            Float billDiscount,
            Float tip,
            Status status,
            List<Long> mealIds) {
        this.id = id;
        this.client = client;
        this.employee = employee;
        this.date = date;
        this.totalCost = totalCost;
        this.billDiscount = billDiscount;
        this.tip = tip;
        this.status = status;
        this.mealIds = mealIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Float totalCost) {
        this.totalCost = totalCost;
    }

    public Float getBillDiscount() {
        return billDiscount;
    }

    public void setBillDiscount(Float billDiscount) {
        this.billDiscount = billDiscount;
    }

    public Float getTip() {
        return tip;
    }

    public void setTip(Float tip) {
        this.tip = tip;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Long> getMealIds() {
        if(mealIds == null){
            return new ArrayList<>();
        }
        return mealIds;
    }

    public void setMealIds(List<Long> mealIds) {
        this.mealIds = mealIds;
    }
}
