package pl.programator.restaurantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.programator.restaurantApp.common.ConstErrorMsg;
import pl.programator.restaurantApp.common.MsgSource;
import pl.programator.restaurantApp.common.ValidationUtils;
import pl.programator.restaurantApp.domain.model.Product;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.ProductService;
import pl.programator.restaurantApp.domain.service.repository.ProductRepository;
import pl.programator.restaurantApp.exception.CommonBadRequestException;
import pl.programator.restaurantApp.exception.CommonConflictException;

import java.util.List;
import java.util.Optional;

import static pl.programator.restaurantApp.common.ValidationUtils.*;

@Service
public class ProductServiceImpl extends AbstractCommonService implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(MsgSource msgSource, ProductRepository productRepository) {
        super(msgSource);
        this.productRepository = productRepository;
    }

    @Override
    public Long createProduct(Product product) {
        validateCreateProductRequest(product);

        Product repositoryProduct =
                productRepository.
                        createProduct(
                                product.getAmount(),
                                product.getName(),
                                product.getUnit());
        return repositoryProduct.getId();
    }

    @Override
    public Optional<Product> getProduct(Long id) {
        Optional productOptional = productRepository.getProduct(id);
        if (!productOptional.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR001);
        }
        return productOptional;
    }


    @Override
    public List<Product> getAllProducts(Pageable pageable) {
        return productRepository.getAllProducts(pageable);
    }

    @Override
    public void removeProduct(Long id) {
        productRepository.removeProduct(id);
    }

    @Override
    public void updateProduct(Long id, Product product) {

        Optional<Product> productOptional = productRepository.getProduct(id);
        if (!productOptional.isPresent()) {
            productRepository.createProduct(product.getAmount(), product.getName(), product.getUnit());
        }

        productOptional.get().setAmount(product.getAmount());
        productOptional.get().setName(product.getName());
        productOptional.get().setUnit(product.getUnit());

    }

    private void validateCreateProductRequest(Product product) {

        if (isNullorEmpty(product.getName()) || isNullorEmpty(product.getUnit())) {
            throw new CommonBadRequestException(msgSource.ERR012);
        }
        if (valueLessThan0(product.getAmount())) {
            throw new CommonBadRequestException(msgSource.ERR006);
        }
        List<Product> products = productRepository.getAllProducts(Pageable.unpaged());
        for (Product p : products
        ) {
            if (p.equals(product)) {
                throw new CommonConflictException(msgSource.ERR016);
            }
        }
    }


}
