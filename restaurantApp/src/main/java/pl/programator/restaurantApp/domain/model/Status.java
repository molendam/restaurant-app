package pl.programator.restaurantApp.domain.model;

public enum Status {
    STARTED, FINISHED
}
