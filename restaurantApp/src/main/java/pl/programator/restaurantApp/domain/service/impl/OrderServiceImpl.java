/*
package pl.programator.restaurantApp.domain.service.impl;


import org.springframework.stereotype.Service;
import pl.programator.restaurantApp.common.MsgSource;
import pl.programator.restaurantApp.domain.model.*;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.OrderService;
import pl.programator.restaurantApp.domain.service.repository.BillRepository;
import pl.programator.restaurantApp.domain.service.repository.ClientRepository;
import pl.programator.restaurantApp.domain.service.repository.EmployeeRepository;
import pl.programator.restaurantApp.exception.CommonBadRequestException;
import pl.programator.restaurantApp.exception.CommonConflictException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class OrderServiceImpl extends AbstractCommonService implements OrderService {


    private Map<Long, Order> orders = new HashMap<>();
    private final EmployeeRepository employeeRepository;
    private final ClientRepository clientRepository;
    private final BillRepository billRepository;

    private static Long orderId= 0l;


    public OrderServiceImpl(
            MsgSource msgSource, EmployeeRepository employeeRepository, ClientRepository clientRepository, BillRepository billRepository) {
        super(msgSource);
        this.employeeRepository = employeeRepository;
        this.clientRepository = clientRepository;
        this.billRepository = billRepository;
    }


    public Bill pay(Long orderId, Long clientId, Float tip) {
        if (!orders.containsKey(orderId)) {
            throw new CommonConflictException(msgSource.ERR017);
        }
        Client client = null;
        Float billDiscount = null;

        if (clientId!=null){
            Optional<Client> optionalClient = clientRepository.getClient(clientId);
            if (optionalClient.isPresent()) {
                client = optionalClient.get();
                billDiscount = client.getDiscount();
            }
        }
        Order order = orders.get(orderId);
        float totalCost = calculateOrderCost(orderId, clientId);

      */
/*  //logika płatności
        boolean paid = true;
        //*//*


      Employee employee = employeeRepository.getEmployee(order.getEmployeeId()).get();

      */
/*  Bill bill = new Bill(
                client,
                employee,
                null,
                totalCost,
                billDiscount,
                tip,
                Status.STARTED,
                order.getMealBills());*//*


        Bill bill= billRepository.createBill(
                client,
                employee,
                null,
                totalCost,
                billDiscount,
                tip,
                Status.FINISHED,
                order.getMealBills());
        return bill;
    }


    public void addToOrder(Long orderId, List<MealBill> mealBills) {
        if (!orders.containsKey(orderId)) {
             throw new CommonConflictException(msgSource.ERR017);
        }
        Order order = orders.get(orderId);
        order.getMealBills().addAll(mealBills);
    }

    @Override
    public Long createOrder(Long employeeId, List<MealBill> mealBills) {
        Optional<Employee> employee = employeeRepository.getEmployee(employeeId);
        if (!employee.isPresent()) {
            throw new CommonConflictException(msgSource.ERR005);
        }
        Order order = new Order(employeeId);
        order.setId(++orderId);
        order.setMealBills(mealBills);
        orders.put(order.getId(), order);
        return order.getId();
    }

    public Float calculateOrderCost(Long orderId, Long clientId) {
        if (!orders.containsKey(orderId)) {
            // throw new CommonConflictException("brak takiego zamówienia");
        }
        Order order = orders.get(orderId);
        List<MealBill> mealBills = order.getMealBills();
        float totalCost = 0;
        for (MealBill mb : mealBills
        ) {
            totalCost += mb.getMealPrice() * mb.getQuantity();
        }
        if (clientId == null){
            return totalCost;
        }
        Optional<Client> optionalClient = clientRepository.getClient(clientId);
        if (optionalClient.isPresent()) {
            totalCost -= totalCost * optionalClient.get().getDiscount();
        }
        return totalCost;
    }
}
*/
