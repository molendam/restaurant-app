package pl.programator.restaurantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Long createProduct(Product product);

    Optional<Product> getProduct(Long id);

    List<Product> getAllProducts(Pageable pageable);

    void removeProduct(Long id);

    void updateProduct(Long id, Product product);
}
 /*   Long createProduct(Float amount,
                       String name,
                       String unit
    );*/