package pl.programator.restaurantApp.domain.service.impl;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.programator.restaurantApp.common.MsgSource;
import pl.programator.restaurantApp.domain.model.Meal;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.MealService;
import pl.programator.restaurantApp.domain.service.repository.MealRepository;
import pl.programator.restaurantApp.exception.CommonBadRequestException;
import pl.programator.restaurantApp.exception.CommonConflictException;

import static pl.programator.restaurantApp.common.ValidationUtils.*;


import java.util.List;
import java.util.Optional;

@Service
public class MealServiceImpl extends AbstractCommonService implements MealService {

    private final MealRepository mealRepository;

    @Autowired
    public MealServiceImpl(MsgSource msgSource, MealRepository mealRepository) {
        super(msgSource);
        this.mealRepository = mealRepository;
    }

    @Override
    public Long createMeal(Meal meal) {
        validateCreateMealRequest(meal);
        return mealRepository.createMeal(meal);
    }

    @Override
    public Meal getMeal(Long id) {
        Optional<Meal> meal = mealRepository.getMeal(id);
        if (!meal.isPresent()) {
            throw new CommonConflictException(msgSource.ERR003);
        }
        return meal.get();
    }

    @Override
    public List<Meal> getAllMeals(Pageable pageable) {
        return mealRepository.getAllMeals(pageable);
    }

    @Override
    public void removeMeal(Long id) {
        mealRepository.removeMeal(id);
    }


    private void validateCreateMealRequest(Meal meal) {

        if (isNullorEmpty(meal.getType()) ||
                isNullorEmpty(meal.getName()) ||
                isNullorEmpty(meal.getUnit())
        ) {
            throw new CommonBadRequestException(msgSource.ERR012);
        }
        if (valueLessThan0(meal.getAmount())) {
            throw new CommonConflictException(msgSource.ERR006);
        }
        if (valueLessThan0(meal.getPrice())) {
            throw new CommonConflictException(msgSource.ERR007);
        }
        List<Meal> meals = mealRepository.getAllMeals(Pageable.unpaged());
        for (Meal m : meals
        ) {
            if (m.equals(meal)) {
                throw new CommonConflictException(msgSource.ERR015);
            }
        }
    }
}



/*@Override
    public Long createMeal(
            String type,
            String name,
            String description,
            Float price,
            Float amount,
            String unit,
            Boolean availabillity) {

        validateCreateMealRequest(type,name,price,amount,unit);
        Meal meal= mealRepository.createMeal(type, name, description, price, amount, unit, availabillity);
        System.out.println(meal.getDescription());
        System.out.println(meal.getId());
        return meal.getId();
    }*/
