package pl.programator.restaurantApp.domain.service.repository;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    Product createProduct(Float amount,
                          String name,
                          String unit
    );
    Optional<Product> getProduct(Long id);

    List<Product> getAllProducts(Pageable pageable);

    void removeProduct(Long id);
}
