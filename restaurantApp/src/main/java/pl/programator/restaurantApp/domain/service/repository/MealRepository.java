package pl.programator.restaurantApp.domain.service.repository;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Meal;

import java.util.List;
import java.util.Optional;


public interface MealRepository {
    Meal createMeal (String type,
                          String name,
                          String description,
                          Float price,
                          Float amount,
                          String unit,
                          Boolean availabillity
    );
    Optional<Meal> getMeal(Long id);

    List<Meal> getAllMeals(Pageable pageable);

    void removeMeal(Long id);

    Long createMeal(Meal meal);
}
