package pl.programator.restaurantApp.domain.service.repository;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BillRepository {

    Bill createBill(
            Long employeeId,
            List<Long> mealIdsList,
            Client client,
            LocalDateTime date,
            Float totalCost,
            Float billDiscount,
            Float tip,
            Status status);

    Optional<Bill> getBill(Long id);

    List<Bill> getAllBills(Pageable pageable);

    void removeBill(Long id);

    Bill save(Bill bill);

}