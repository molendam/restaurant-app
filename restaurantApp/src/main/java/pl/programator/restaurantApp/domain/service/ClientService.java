package pl.programator.restaurantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Client;
import pl.programator.restaurantApp.domain.model.Position;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Long createClient(Client client);

    Optional<Client> getClient(Long id);

    List<Client> getAllClients(Pageable pageable);

    void removeClient(Long id);
}

/* Long createClient (String firstName, String surname, Float discount);*/
