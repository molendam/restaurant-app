package pl.programator.restaurantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.ValidationUtils;
import pl.programator.restaurantApp.common.MsgSource;
import pl.programator.restaurantApp.domain.model.Client;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.ClientService;
import pl.programator.restaurantApp.domain.service.repository.ClientRepository;
import pl.programator.restaurantApp.exception.CommonBadRequestException;
import pl.programator.restaurantApp.exception.CommonConflictException;

import java.util.List;
import java.util.Optional;

import static pl.programator.restaurantApp.common.ValidationUtils.*;


@Service
public class ClientServiceImpl extends AbstractCommonService implements ClientService {

    public final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(MsgSource msgSource, ClientRepository clientRepository) {
        super(msgSource);
        this.clientRepository = clientRepository;

    }

    @Override
    public Long createClient(Client client) {

        validateClientCreateRequest(client);

        return clientRepository.createClient(
                client.getFirstName(),
                client.getSurname(),
                client.getDiscount()).getId();
    }

    @Override
    public Optional<Client> getClient(Long id) {
        Optional<Client> client = clientRepository.getClient(id);
        if (!client.isPresent()) {
            throw new CommonConflictException(msgSource.ERR002);
        }
        return client;
    }

    @Override
    public List<Client> getAllClients(Pageable pageable) {
        return clientRepository.getAllClients(pageable);
    }

    @Override
    public void removeClient(Long id) {
        clientRepository.removeClient(id);
    }

    private void validateClientCreateRequest(Client client) {
        if (isNull(client.getDiscount()) ||
                isNullorEmpty(client.getFirstName()) ||
                isNullorEmpty(client.getSurname())) {
            throw new CommonBadRequestException(msgSource.ERR012);
        }
        float discount = client.getDiscount();
        if (discount < 0 || discount > 1.0) {
            throw new CommonBadRequestException(msgSource.ERR011);
        }
        for (Client client1:clientRepository.getAllClients(Pageable.unpaged())) {
            if(client.equals(client1)){
                throw new CommonConflictException(msgSource.ERR013);
            }

        }
    }


}
