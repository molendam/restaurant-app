package pl.programator.restaurantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.programator.restaurantApp.common.MsgSource;
import pl.programator.restaurantApp.domain.model.*;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.BillService;
import pl.programator.restaurantApp.domain.service.repository.BillRepository;
import pl.programator.restaurantApp.domain.service.repository.ClientRepository;
import pl.programator.restaurantApp.domain.service.repository.EmployeeRepository;
import pl.programator.restaurantApp.domain.service.repository.MealRepository;
import pl.programator.restaurantApp.exception.CommonBadRequestException;
import pl.programator.restaurantApp.exception.CommonConflictException;

import java.util.*;

@Service
public class BillServiceImpl extends AbstractCommonService implements BillService {
    private final BillRepository billRepository;
    private final EmployeeRepository employeeRepository;
    private final MealRepository mealRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public BillServiceImpl(
            MsgSource msgSource,
            BillRepository billRepository,
            EmployeeRepository employeeRepository,
            MealRepository mealRepository,
            ClientRepository clientRepository) {
        super(msgSource);
        this.billRepository = billRepository;
        this.employeeRepository = employeeRepository;
        this.mealRepository = mealRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public Optional<Bill> getBill(Long id) {
        Optional<Bill> bill = billRepository.getBill(id);
        if (!bill.isPresent()) {
            throw new CommonConflictException(msgSource.ERR004);
        }
        return bill;
    }

    @Override
    public List<Bill> getAllBills(Pageable pageable) {
        return billRepository.getAllBills(pageable);
    }

    @Override
    public Long createBill(List<Long> mealIdsList, Long employeeId) {


        validateCreateBillRequest(
                mealIdsList,
                employeeId);

        Optional<Employee> optionalEmployee = employeeRepository.getEmployee(employeeId);
        if (!optionalEmployee.isPresent()){
            throw new CommonConflictException(msgSource.ERR005);
        }

        List<Meal> meals = extractMealsFromIdList(mealIdsList);
        float totalCost = calculateCostWithoutDiscount(meals);
        return billRepository.createBill(
                employeeId,
                mealIdsList,
                null,
                null,
                totalCost,
                0f,
                0f,
                Status.STARTED).getId();
    }

    @Override
    public void addMeals(List<Long> mealIdsList, Long billId) {

        Optional<Bill> billOptional = billRepository.getBill(billId);
        validateAddMealsRequest(mealIdsList, billOptional.isPresent());

        //billOptional is checked, and than
        Bill bill = billOptional.get();
        bill.getMealIds().addAll(mealIdsList);
        List<Meal> meals = extractMealsFromBill(bill);
        bill.setTotalCost(calculateCost(bill.getClient(),meals));
        //bill.setTotalCost(calculateTotalCost(extractMealsFromBill(bill)));
        //zmieniam!
        billRepository.save(bill);
    }

    @Override
    public void removeBill(Long id) {
        billRepository.removeBill(id);
    }

    @Override
    public Bill pay(Long billId, Long clientId, Float tip) {

        Optional<Bill> billOptional = billRepository.getBill(billId);
        validatePayRequest(billOptional, clientId);

        Optional<Client> clientOptional =
                clientId ==null?
                        Optional.empty():clientRepository.getClient(clientId);

        Bill bill = billOptional.get();

        Float totalCost = calculateCostWithoutDiscount(extractMealsFromBill(bill));

        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();
            totalCost = calculateCostAfterDiscount(totalCost, client.getDiscount());
            bill.setClient(client);
            bill.setBillDiscount(client.getDiscount());
            bill.setTip(tip);
        }
        bill.setTotalCost(totalCost);
        bill.setStatus(Status.FINISHED);
        billRepository.save(bill);
        return bill;
    }


    //--------------------------------------------------------------------------------------------//
    //############################ Utils #########################################
   /* private Client clientIfExit(Long clientId) {
        Optional<Client> optionalClient = clientRepository.getClient(clientId);
        return optionalClient.orElse(null);
    }*/

    private float calculateCost(Client client, List<Meal> meals){
        float withoutDiscount = calculateCostWithoutDiscount(meals);
        return client == null?
                withoutDiscount:calculateCostAfterDiscount(withoutDiscount,client.getDiscount());
    }
    private float calculateCostAfterDiscount(Float totalCost, Float discount) {
        if (discount != null) {
            totalCost -= totalCost * discount;
        }
        return totalCost;
    }
    private float calculateCostWithoutDiscount(List<Meal> meals) {
        return meals.
                stream().
                map(Meal::getAmount).
                reduce(0f,(Float::sum));
    }

    private List<Meal> extractMealsFromBill(Bill bill) {
        /* for (Long mealId : bill.getMealIds()
        ) {
            if (mealRepository.getMeal(mealId).isPresent()) {
                meals.add(mealRepository.getMeal(mealId).get());
            }
        }*/
        return extractMealsFromIdList(bill.getMealIds());
    }

    private List<Meal> extractMealsFromIdList(List<Long> mealIdsList) {
        List<Meal> meals = new ArrayList<>();
        mealIdsList.forEach(x->addMealIfExist(meals,x));
        return meals;
    }
    //--------------------------------------------------------------------------------------------//
    //############################ Validators #########################################
    private void validateAddMealsRequest(List<Long> mealIdsList, Boolean billIsPresent) {
        if (!billIsPresent) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }
        if (isEmptyOrNull(mealIdsList)) {
            throw new CommonBadRequestException(msgSource.ERR017);
        }
        mealsExist(mealIdsList) ;
    }
    private void addMealIfExist(List<Meal> meals, Long mealId){
        Optional<Meal> optionalMeal = mealRepository.getMeal(mealId);
        optionalMeal.ifPresent(meals::add);
    }

    private void validateCreateBillRequest(List<Long> mealIdsList, Long employeeId) {
        if (!isEmployeeExist(employeeId)) {
            throw new CommonConflictException(msgSource.ERR005);
        }
        if (isEmptyOrNull(mealIdsList)) {
            throw new CommonBadRequestException(msgSource.ERR017);
        }
        mealsExist(mealIdsList);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private void validatePayRequest(Optional<Bill> bill, Long clientId) {
        if (!bill.isPresent()) {
            throw new CommonBadRequestException(msgSource.ERR004);
        }
        if (bill.get().getStatus()==Status.FINISHED){
            throw new CommonConflictException(msgSource.ERR018);
        }
        if (clientId != null && !clientRepository.getClient(clientId).isPresent()) {
            throw new CommonConflictException(msgSource.ERR002);
        }
    }

    private boolean isEmployeeExist(Long employeeId) {
        return employeeRepository.getEmployee(employeeId).isPresent();
    }

    private boolean isEmptyOrNull(List<Long> mealIdsList) {
        return mealIdsList == null || mealIdsList.isEmpty();
    }

    private void mealsExist(List<Long> mealIdsList) {
        mealIdsList.
                stream().
                map(mealId -> mealRepository.getMeal(mealId).isPresent()).
                forEach(exist ->{
                    if(!exist){
                       throw  new CommonConflictException(msgSource.ERR003);
                    }
                });
/*
        for (Long id : mealIdsList
        ) {
            if (!mealRepository.getMeal(id).isPresent()) {
                throw new CommonConflictException(msgSource.ERR003);
            }
        }*/
    }

    private boolean clientExist(Long clientId){return clientRepository.getClient(clientId).isPresent();
    }

}
