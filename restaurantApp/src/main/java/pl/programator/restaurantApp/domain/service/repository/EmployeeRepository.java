package pl.programator.restaurantApp.domain.service.repository;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.domain.model.Employee;
import pl.programator.restaurantApp.domain.model.Position;
import pl.programator.restaurantApp.infrastructure.entity.EmployeeHibernate;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {
    Employee createEmployee (
                             Position position,
                             String firstName,
                             String lastName,
                             String pesel,
                             Long managerId,
                             Float salary);

    Optional<Employee> getEmployee(Long id);

    List<Employee> getAllEmployees(Pageable pageable);

    void removeEmployee(Long id);
}
