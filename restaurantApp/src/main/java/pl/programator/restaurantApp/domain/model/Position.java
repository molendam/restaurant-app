package pl.programator.restaurantApp.domain.model;

public enum Position {
    GENERAL_MANAGER,
    ASSISTANT_MANAGER,
    LINE_COOK,
    DISHWASHER,
    KITCHEN_MANAGER,
    WAITER,
    TEAM_LEADER
}
