package pl.programator.restaurantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.programator.restaurantApp.controller.dto.BillDTO;
import pl.programator.restaurantApp.domain.model.Bill;

import java.util.List;
import java.util.Optional;

public interface BillService {

    Long createBill(List<Long> mealIdsList, Long employeeId);

    Optional<Bill> getBill(Long id);

    List<Bill> getAllBills(Pageable pageable);

    void removeBill(Long id);

    void addMeals(List<Long> mealIdsList, Long billId);

    Bill pay(Long billId, Long clientId, Float tip);
}














  /*  Long createBill (
            Employee employee,
            LocalDateTime date,
            Float totalCost,
            Float billDiscount,
            Float tip,
            Status status,
            List<MealBill> mealBills
    );*/