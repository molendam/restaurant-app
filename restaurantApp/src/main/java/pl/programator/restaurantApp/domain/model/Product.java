package pl.programator.restaurantApp.domain.model;


import java.util.Objects;

public class Product {

    private Long id;
    private Float amount;
    private String name;
    private String unit;
    public Product(Long id, Float amount, String name, String unit) {
        this.id = id;
        this.amount = amount;
        this.name = name;
        this.unit = unit;
    }

    public Product(Float amount, String name, String unit) {
        this.amount = amount;
        this.name = name;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getName().equals(product.getName()) &&
                Objects.equals(getUnit(), product.getUnit());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getUnit());
    }
}
