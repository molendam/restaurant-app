package pl.programator.restaurantApp.domain.service.impl;

import org.hibernate.action.spi.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.programator.restaurantApp.common.MsgSource;
import pl.programator.restaurantApp.domain.model.Employee;
import pl.programator.restaurantApp.domain.model.Position;
import pl.programator.restaurantApp.domain.service.AbstractCommonService;
import pl.programator.restaurantApp.domain.service.EmployeeService;
import pl.programator.restaurantApp.domain.service.repository.EmployeeRepository;
import pl.programator.restaurantApp.exception.CommonBadRequestException;
import pl.programator.restaurantApp.exception.CommonConflictException;

import java.util.List;
import java.util.Optional;

import static pl.programator.restaurantApp.common.ValidationUtils.*;

@Service
public class EmployeeServiceImpl extends AbstractCommonService implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(MsgSource msgSource, EmployeeRepository employeeRepository) {
        super(msgSource);
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Optional<Employee> getEmployee(Long id) {
        Optional<Employee> employee = employeeRepository.getEmployee(id);
        if (!employee.isPresent()) {
            throw new CommonConflictException(msgSource.ERR005);
        }
        return employee;
    }

    @Override
    public List<Employee> getAllEmployees(Pageable pageable) {
        List<Employee> employees = employeeRepository.getAllEmployees(pageable);
        return employees;
    }

    @Override
    public Long createEmployee(Employee employee) {
        validateCreateEmployeeRequest(employee);
        return employeeRepository.createEmployee(
                employee.getPosition(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getPesel(),
                employee.getManagerId(),
                employee.getSalary()).getId();
    }

    @Override
    public void removeEmployee(Long id) {
        employeeRepository.removeEmployee(id);
    }

    private void validateCreateEmployeeRequest(Employee employee) {
        if (isNullorEmpty(employee.getFirstName()) ||
                isNullorEmpty(employee.getLastName()) ||
                isNullorEmpty(employee.getPesel()) ||
                isNull(employee.getPosition())) {
            throw new CommonBadRequestException(msgSource.ERR012);
        }
        //if ((Long.parseLong(employee.getPesel()))=> )
        if (employee.getPesel().length() != 11) {
            throw new CommonConflictException(msgSource.ERR010);
        }
        if (valueLessThan0(employee.getSalary())) {
            throw new CommonConflictException(msgSource.ERR008);
        }
        for (Employee employee1 : employeeRepository.getAllEmployees(Pageable.unpaged())) {
            if (employee.equals(employee1)) {
                throw new CommonConflictException(msgSource.ERR014);
            }
        }
    }
}
