package pl.programator.restaurantApp.domain.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Employee {

    private Long id;
    private String firstName;
    private String lastName;
    private String pesel;
    private List<Long> employees;
    private Long managerId;
    private List<Long> billIds;
    private Position position;
    private Float salary;

    public Employee(
            Long id,
            String firstName,
            String lastName,
            String pesel,
            Long managerId,
            List<Long> billIds,
            Position position,
            Float salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.managerId = managerId;
        this.billIds = billIds;
        this.position = position;
        this.salary = salary;
    }

    public Employee(
            String firstName,
            String lastName,
            String pesel,
            Long managerId,
            List<Long> billIds,
            Position position,
            Float salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.managerId = managerId;
        this.billIds = billIds;
        this.position = position;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public List<Long> getEmployees() {
        if (employees == null) {
            return new ArrayList<Long>();
        }
        return employees;
    }

    public void setEmployees(List<Long> employees) {
        this.employees = employees;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public List<Long> getBillIds() {
        return billIds;
    }

    public void setBillIds(List<Long> billIds) {
        if(billIds ==null){
            billIds = new ArrayList<>();
        }
        this.billIds = billIds;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(getPesel(), employee.getPesel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPesel());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Employee.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("pesel='" + pesel + "'")
                .add("employees=" + employees)
                .add("managerId=" + managerId)
                .add("billIds=" + billIds)
                .add("position=" + position)
                .add("salary=" + salary)
                .toString();
    }
}
