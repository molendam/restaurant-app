package pl.programator.restaurantApp.domain.model;

import java.util.List;
import java.util.Objects;

public class Meal {


    private Long id;
    private String type;
    private String name;
    private String description;
    private Float price;
    private Float amount;
    private String unit;
    private Boolean availabillity;
    public Meal() {
    }

    public Meal(
            String type,
            String name,
            String description,
            Float price,
            Float amount,
            String unit,
            Boolean availabillity) {
        this.type = type;
        this.name = name;
        this.description = description;
        this.price = price;
        this.amount = amount;
        this.unit = unit;
        this.availabillity = availabillity;
    }

    public Meal(
            Long id,
            String type,
            String name,
            String description,
            Float price,
            Float amount,
            String unit,
            Boolean availabillity) {

        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.price = price;
        this.amount = amount;
        this.unit = unit;
        this.availabillity = availabillity;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getAvailabillity() {
        return availabillity;
    }

    public void setAvailabillity(Boolean availabillity) {
        this.availabillity = availabillity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meal)) return false;
        Meal meal = (Meal) o;
        return Objects.equals(getType(), meal.getType()) &&
                getName().equals(meal.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getName());
    }
}
