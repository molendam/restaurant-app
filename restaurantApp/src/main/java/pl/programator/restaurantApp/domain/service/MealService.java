package pl.programator.restaurantApp.domain.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.programator.restaurantApp.domain.model.Meal;

import java.util.List;
import java.util.Optional;


public interface MealService {

    Long createMeal(Meal meal);

    Meal getMeal(Long id);

    List<Meal> getAllMeals(Pageable pageable);

    void removeMeal(Long id);
}

/*

    Long createMeal (String type,
                     String name,
                     String description,
                     Float price,
                     Float amount,
                     String unit,
                     Boolean availabillity
    );*/
