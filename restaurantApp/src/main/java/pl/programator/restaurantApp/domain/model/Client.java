package pl.programator.restaurantApp.domain.model;

import javax.persistence.Column;
import java.util.Objects;

public class Client {

    private Long id;
    private String firstName;
    private String surname;
    private Float discount;

    public Client(String firstName, String surname, Float discount) {
        this.firstName = firstName;
        this.surname = surname;
        this.discount = discount;
    }

    public Client(Long id, String firstName, String surname, Float discount) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(getFirstName(), client.getFirstName()) &&
                Objects.equals(getSurname(), client.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getSurname());
    }
}
