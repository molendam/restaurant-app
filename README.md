# Restaurant App
Application demo for restaurant, using tools technology:
 - Spring 
 - Spring Boot
 - Spring Data
 - Hibernate
 - Maven
 - PostgreSQL
 - Git
 - GitLab


# Project Structure

```
├───main
│   ├───java
│   │   └───pl
│   │       └───programator
│   │           └───restaurantApp
│   │               ├───common
│   │               ├───controller
│   │               │   └───dto
│   │               ├───domain
│   │               │   ├───model
│   │               │   └───service
│   │               │       ├───impl
│   │               │       └───repository
│   │               ├───exception
│   │               └───infrastructure
│   │                   ├───adapter
│   │                   ├───entity
│   │                   └───repository
│   └───resources
└───test
    └───java
        └───pl
            └───programator
                └───restaurantApp
```



## API

### /client


#### /client/getAll
-   `GET`  : Get all clients
#### /client/get/:id
-   `GET`  : Get client
#### /client/add
- `POST`  : Create a new client
#### /client/delete/:id
-   `DELETE`  : Delete a client

### /employee

#### /employee/getAll
-   `GET`  : Get all employees
#### /employee/get/:id
-   `GET`  : Get employee
#### /employee/add
- `POST`  : Create a new employee
#### /employee/delete/:id
-   `DELETE`  : Delete a employee

### /meal

#### /meal/getAll
-   `GET`  : Get all meals
#### /meal/get/:id
-   `GET`  : Get meal
#### /meal/add
- `POST`  : Create a new meal
#### /meal/delete/:id
-   `DELETE`  : Delete a meal
### /product

#### /product/getAll
-   `GET`  : Get all products
#### /product/get/:id
-   `GET`  : Get product
#### /product/add
- `POST`  : Create a new product
#### /product/delete/:id
-   `DELETE`  : Delete a product
### /bill

#### /bill/create
- `POST`  : Create a new bill

#### /bill/get/:id
-   `GET`  : Get bill
#### /bill/getAll
-   `GET`  : Get all bills
#### /bill/delete/:id
-   `DELETE`  : Delete a bill

#### /bill/addMeals
- `PUT`  : Add products to bill
#### /bill/pay
- `POST`  : Pay for a bill

